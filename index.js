const fs = require("fs").promises;

const puppeteer = require("puppeteer");
const rxjs = require("rxjs");
const { mergeMap, toArray } = require("rxjs/operators");

const logger = require("./logger.js");

const toTitleCase = (phrase) => {
  return phrase
    .toLowerCase()
    .split(" ")
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(" ");
};

exports.readNotLocatedFile = async function readNotLocatedFile() {
  const fileContents = await fs.readFile("./not-located.txt");
  let docNumbers;
  try {
    docNumbers = fileContents.toString().split("\n");
    if (docNumbers.length === 0) {
      throw new Error();
    }
  } catch {
    throw new Error(
      'Please add an array of DOC numbers to the not-located.json file. Please see the README\'s "Data setup" section for an example.'
    );
  }

  return docNumbers;
};

exports.writeToLocatedFile = async function writeToLocatedFile(locatedPeople) {
  await fs.writeFile("./located.json", JSON.stringify(locatedPeople, null, 2));
};

exports.getCurrentLocations = async function getCurrentLocations(docNumbers) {
  const locatedPeople = [];
  let errors = [];
  const chunksOfDocNumbers = [];
  const size = 50;

  for (let i = 0; i < docNumbers.length; i += size) {
    chunksOfDocNumbers.push(docNumbers.slice(i, i + size));
  }

  const browser = await puppeteer.launch();
  const options = { timeout: 10000 };

  let page = await browser.newPage();
  await page.goto("https://appsdoc.wi.gov/lop/home.do");

  const iAgreeButtonSelector = 'input[name="OKButton2"]';
  await page.waitForSelector(iAgreeButtonSelector, options);
  await page.click(iAgreeButtonSelector);
  page.close();

  const locatePeople = async (docNumbers) => {
    logger.debug(`People left to locate: ${docNumbers.length}`);
    if (docNumbers.length === 0) {
      return;
    }

    let page = await browser.newPage();
    await page.goto("https://appsdoc.wi.gov/lop/home.do");

    const docNumber = docNumbers.pop();

    try {
      const docNumSelector = 'input[name="DOC_NUM"]';
      await page.waitForSelector(docNumSelector, options);
      await page.type(docNumSelector, docNumber.toString());
      const searchLinkSelector = 'a[title="Click here to perform the search"]';
      await page.waitForSelector(searchLinkSelector, options);
      await page.click(searchLinkSelector);

      try {
        const errorMessageSelector = ".errormessage";
        await page.waitForSelector(errorMessageSelector, { timeout: 2000 });
        locatedPeople.push({ docNumber, location: "Not Listed" });
        await page.evaluate((docNumSelector) => {
          document.querySelector(docNumSelector).value = "";
        }, docNumSelector);
        await locatePeople(docNumbers);
        return;
      } catch {}

      const nameSelector = '.XPBody[colspan="2"]';
      await page.waitForSelector(nameSelector);
      const name = await page.evaluate(
        (nameSelector) =>
          document
            .querySelector(nameSelector)
            .textContent.replace("Name:", "")
            .trim(),
        nameSelector
      );

      const statusTabSelector = 'img[src="/lop/images/Status1Tab.gif"]';
      try {
        await page.waitForSelector(statusTabSelector, options);
        await page.click(statusTabSelector);
      } catch {
        await page.evaluate((docNumSelector) => {
          document.querySelector(docNumSelector).value = "";
        }, docNumSelector);
        await locatePeople(docNumbers);
        return;
      }

      let institutionSelector = 'a[href="javascript:showInstitution()"]';
      try {
        await page.waitForSelector(institutionSelector, options);
      } catch {
        institutionSelector =
          "body > table > tbody > tr:nth-child(47) > td > div > table > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td.XPBody > table > tbody > tr:nth-child(1) > td > span.FormTextData";
      }
      const institution = await page.evaluate(
        (institutionSelector) =>
          document.querySelector(institutionSelector).textContent.trim(),
        institutionSelector
      );
      const titleCaseInstitution = toTitleCase(institution);
      locatedPeople.push({
        docNumber,
        location:
          titleCaseInstitution === "Incarcerated"
            ? "Not Listed"
            : titleCaseInstitution,
        name,
      });

      await page.click("a[href=\"javascript:execNavCommand('newsearch');\"]");
      await locatePeople(docNumbers);
      page.close();
      return;
    } catch (error) {
      const iAgreeButton = await page.$(iAgreeButtonSelector);
      if (iAgreeButton) {
        await page.click(iAgreeButtonSelector);
        await locatePeople([docNumber, ...docNumbers]);
        return;
      }

      logger.error(error);
      errors.push(docNumber);

      await page.goto("https://appsdoc.wi.gov/lop/home.do");
      await locatePeople(docNumbers);
      return;
    }
  };

  const locatePeopleInParallel = async (chunksOfDocNumbers) =>
    await rxjs
      .from(chunksOfDocNumbers)
      .pipe(
        mergeMap(async (docNumbers) => {
          const page = await browser.newPage();
          return await locatePeople(docNumbers);
        }, 5),
        toArray()
      )
      .toPromise();

  try {
    await locatePeopleInParallel(chunksOfDocNumbers);

    if (errors.length > 0) {
      const chunksOfDocNumbersErrored = [];
      for (let i = 0; i < errors.length; i += size) {
        chunksOfDocNumbersErrored.push(errors.slice(i, i + size));
      }
      errors = [];
      await locatePeopleInParallel(chunksOfDocNumbersErrored);
    }

    await browser.close();
    return locatedPeople;
  } catch (error) {
    logger.error(error);
    process.exit(1);
  }
};
