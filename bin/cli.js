#!/usr/bin/env node

const {
  getCurrentLocations,
  readNotLocatedFile,
  writeToLocatedFile,
} = require("../index.js");
const logger = require("../logger.js");

(async () => {
  const docNumbers = await readNotLocatedFile();
  const locatedPeople = await getCurrentLocations(docNumbers);
  writeToLocatedFile(locatedPeople);
  logger.info("Done!");
})();
