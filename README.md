# Finding WI DOC incarcerated people's locations #

This is a Node app that will find the current location of people incarcerated by the Wisconsin Department of Corrections. It does this by reading the incarcerated people's data that you enter into the `not-located.json` file, scraping the WI DOC "Offender Locator" site using those data, and then outputting an updated version of the data with current locations to the `located.json` file.

## Getting started ##

### Installation ###

Clone this repository and then install the app's dependencies by running the command:

```bash
npm i
```

### Data setup ###

An array of DOC numbers needs to be added to the `not-located.json` file for the incarcerated people whose locations need to be found.

For example:
```json
[100001, 100002, 100003]
```

### Running the app ###

To run the app, enter the command:

```bash
npm start
```

## Results ##

The app will output data to the `located.json` file.

For example:

```json
[
  {
    "docNumber": 100001,
    "location": "Columbia Correctional Institution"
  },
  {
    "docNumber": 100002,
    "location": "Fox Lake Correctional Institution"
  },
  {
    "docNumber": 100003,
    "location": "Waupun Correctional Institution"
  }
]
```

### Not found, not listed, and errors ###

In some cases, locations will not be added to the results:

- If a person is not found in the system their location will be listed as `"Not found"`
- If a person is found with no institution listed their location will be listed as `"Not listed"`
- If the app runs into an error while finding a person's location their location will be listed as `"Error locating person with DOC# {docNumber}`
    - In this case, you can either re-run the app with the DOC numbers that resulted in errors or search with the DOC number manually on the "Offender Locator" site